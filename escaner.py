import socket
import struct 
from hexdump import hexdump
import sys
import os
import pickle

def desempaquetar(paquete):
    destino, origen, protocolo = struct.unpack('! 6s 6s H', paquete[:14]) 
    return obtener_mac(destino), obtener_mac(origen), socket.htons(protocolo), paquete[14:]
 
def obtener_mac(mac_sf):
    bytes_str = map('{:02x}'.format, mac_sf)
    mac = ':'.join(bytes_str).upper()
    return mac
           
def cabecera_ipv4(datos):
    version, dscp, longitud, identificador, banderas, tiempo_vida, protocolo, sum_comp, IP_origen, IP_destino = struct.unpack('!B B H H H B B H 4s 4s', datos[:20])
    return (version, dscp, longitud, identificador, banderas, tiempo_vida, protocolo, sum_comp, ipv4(IP_origen), ipv4(IP_destino), datos[20:])
    
def ipv4(direccion):
    return '.'.join(map(str, direccion))
    
def paquete_icmp4(datos):
    tipo, codigo, checksum = struct.unpack('! B B H', datos[:4])
    return tipo, codigo, checksum, hexdump(datos[4:])
        
def paquete_udp(datos):
    puerto_origen, puerto_destino, longitud, suma = struct.unpack('! H H H H', datos[:8])
    return puerto_origen, puerto_destino, longitud, suma, hexdump(datos[8:])

def paquete_tcp(datos):
    (puerto_origen, puerto_destino, secuencia, num_confirmacion,
     conjunto_banderas) = struct.unpack('! H H L L H', datos[:14])
    
    banderas = (conjunto_banderas >> 12) * 4
    urg = (conjunto_banderas & 32) >> 5
    ack = (conjunto_banderas & 16) >> 4
    psh = (conjunto_banderas & 8) >> 3
    rst = (conjunto_banderas & 4) >> 2
    syn = (conjunto_banderas & 2) >> 1
    fin = conjunto_banderas & 1
    
    return (puerto_origen, puerto_destino, secuencia, num_confirmacion, urg,
            ack, psh, rst, syn, fin, hexdump(datos[banderas:]))

def cabecera_ipv6(datos):
    version, carga_util, cabe_sig, limite_saltos, ip_origen, ip_destino = struct.unpack('!4s H B B 16s 16s', datos[:40])
    return (hex(int.from_bytes(version, byteorder='big')), carga_util, cabe_sig, limite_saltos, str(socket.inet_ntop(socket.AF_INET6,ip_origen)), str(socket.inet_ntop(socket.AF_INET6,ip_destino)), datos[40:])
    
def paquete_icmp6(datos):
    tipo, codigo, sum_comp = struct.unpack('!B B H', datos[:4])
    return (tipo, codigo, sum_comp, hexdump(datos[4:]))

def tipo_icmp6(tipo):
    
    if (tipo == 0):
        return ("Reservado")
    elif (tipo == 1):
        return ("Destino Inalcanzable")
    elif (tipo == 2):
        return ("Paquete demasiado grande")
    elif (tipo == 3):
        return ("Tiempo excedido")
    elif (tipo == 4):
        return ("Problema de Parámetros")
    elif (tipo == 128):
        return ("Solicitud de eco")
    elif (tipo == 129):
        return ("Respuesta de eco")
    elif (tipo == 130):
        return ("Solicitud de registro de multidifusión")
    elif (tipo == 131):
        return ("Reporte de registro de multidifusión")
    elif (tipo == 132):
        return ("Fin de registro de multidifusión")
    elif (tipo == 133):
        return ("Solicitud de enrutador")
    elif (tipo == 134):
        return ("Anuncio de enrutador")
    elif (tipo == 135):
        return ("Solicitud de vecino")
    elif (tipo == 136):
        return ("Anuncio de vecino")
    elif (tipo == 137):
        return ("Mensaje redirigido")
    elif (tipo == 138):
        return ("Renumeración de los enrutadores")
    elif (tipo == 139):
        return ("Solicitud de información de nodo ICMP")
    elif (tipo == 140):
        return ("Respuesta de información de nodo ICMP")
    elif (tipo == 141):
        return ("Solicitud de vecino inverso")
    elif (tipo == 142):
        return ("Anuncio de vecino inverso")
    elif (tipo == 144):
        return ("Descubrimiento del agente base - Solicitud")
    elif (tipo == 145):
        return ("Descubrimiento del agente base - Respuesta")
    elif (tipo == 146):
        return ("Solicitud del prefijo móvil")
    elif (tipo == 147):
        return ("Anuncio del prefijo móvil")
    else:
        return ("Otro")
        
def tipo_icmp4(tipo):
    if (tipo == 0):
        return ("Respuesta de Eco - Ping")
    elif (tipo== 1 ):
        return ("Reservado")
    elif (tipo== 2):
        return ("Reservado")
    elif (tipo== 3):
        return ("Destino Inalcanzable")
    elif (tipo== 4):
        return ("Source Quench (Obsoleto)")
    elif (tipo== 5):
        return ("Redirección")
    elif (tipo== 6):
        return ("Dirección de Host Alternativa (obsoleto)")
    elif (tipo== 7):
        return ("Reservado")
    elif (tipo== 8):
        return ("Solicitud de Eco - Ping")
    elif (tipo== 9):
        return ("Anuncio de Router")
    elif (tipo== 10):
        return ("Solicitud de Router")
    elif (tipo== 11):
        return ("Tiempo Excedido")
    elif (tipo== 12):
        return ("Problema de parámetro")
    elif (tipo== 13):
        return ("Marca de tiempo")
    elif (tipo== 14):
        return ("Respuesta de marca de tiempo")
    elif (tipo == 15):
        return ("Petición de información")
    elif (tipo == 16):
        return ("Respuesta de información")
    elif (tipo == 17):
        return ("Petición de máscara de dirección")
    elif (tipo == 18):
        return ("Respuesta de máscara de dirección")
    elif (tipo == 19):
        return ("Reservado para Seguridad")
    elif (tipo >= 20 and tipo <= 29):
        return ("Reservado para experimentos de robustez")
    elif (tipo == 30):
        return ("Traceroute")
    elif (tipo == 31):
        return ("Error de conversión de datagrama")
    elif (tipo == 32):
        return ("Redirección de Host Móvil")
    elif (tipo == 33):
        return ("IPv6")
    elif (tipo == 34):
        return ("Petición de registro móvil")
    elif (tipo == 35):
        return ("Respuesta de registro móvil")
    elif (tipo == 36):
        return ("Petición de nombre de Dominio")
    elif (tipo == 37):
        return ("Respuesta de nombre de Dominio")
    elif (tipo == 38):
        return ("SKIP Protocolo de Algoritmo de Descubrimiento")
    elif (tipo == 39):
        return ("Photuris, Fallas de Seguridad")
    elif (tipo >= 40 and tipo <= 255):
        return ("Reservado")
    else:
        return ("Desconocido")

        
def paquete_arp(datos):
        tipo_hardware, tipo_protocolo, longitud_hw, longitud_prot, operacion = struct.unpack('!H H B B H',datos[:8])
        if (operacion == 1):
                operacion = 'Solicitud (1)'
        else:
                operacion = 'Respuesta (2)'
        datos = datos[8:]   
        if (tipo_protocolo == 2048):
                dir_hw_origen, IP_hw_origen, dir_hw_destino, IP_hw_destino = struct.unpack('!6s 4s 6s 4s', datos [:20])
                tipo_protocolo = 'IPv4'
                dir_hw_origen = obtener_mac(dir_hw_origen)
                IP_hw_origen = ipv4(IP_hw_origen)
                dir_hw_destino = obtener_mac(dir_hw_destino)
                IP_hw_destino = ipv4(IP_hw_destino)
        else:
                dir_hw_origen = 'Desconocido'
                IP_hw_origen = 'Desconocido'
                dir_hw_destino = 'Desconocido'
                IP_hw_destino = 'Desconocido'
                
        return (tipo_hardware, tipo_protocolo, longitud_hw, longitud_prot, operacion, dir_hw_origen, IP_hw_origen, dir_hw_destino, IP_hw_destino )


