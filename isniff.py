from PyQt5.QtWidgets import (QDialog, QApplication, QMainWindow, QTreeWidgetItem,
     QWidget, QMessageBox, QFileDialog)
from PyQt5 import QtCore, QtGui, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from escaner import *
from hexdump import hexdump
import pickle
import sys
import socket
      
class Principal(QMainWindow):
    iniciado = False
    seleccion = 0
    def __init__(self):
        self.paquetes = {}
        self.seleccion = 0
        self.iniciado = False
        QMainWindow.__init__(self)
        uic.loadUi("interfaces/menu.ui",self)
        self.treeWidget.setColumnWidth(0, 50)
        self.treeWidget.setColumnWidth(1, 200)
        self.treeWidget.setColumnWidth(2, 200)
        self.treeWidget.setColumnWidth(3, 100)
        self.treeWidget.setColumnWidth(4, 100)
        self.limpiar_tabla()
        self.capturar.setToolTip('Iniciar captura de paquetes')
        self.capturar.clicked.connect(self.iniciar_escaner )    
        self.detener.clicked.connect(self.detener_escaner)
        self.treeWidget.doubleClicked.connect(self.tabla_doble_clic)
        self.detener.hide()
        self.reiniciar.hide()
        self.reiniciar.clicked.connect(self.limpiar_tabla)
        self.actionSalir.triggered.connect(self.salir_principal)
        self.actionGuardar.triggered.connect(self.guardar_archivo)
        self.bFiltrar.clicked.connect(self.aplicar_filtro)
        self.actionCargar.triggered.connect(self.abrir_archivo)
        self.filtros.hide()
        self.AcercaDe.triggered.connect(self.acerca_de)

    def acerca_de(self):
        self.acerca = AcercaDe()
        self.acerca.exec_()
        
    def tabla_doble_clic(self, index):
        item = self.treeWidget.selectedIndexes()[0]
        seleccion = item.data(0)
        self.seleccion = seleccion
        self.cargar_visor()

    def detener_escaner(self):
        global iniciado
        self.iniciado = False
        self.detener.hide()
        self.capturar.show()
        self.filtros.show()
    
    def limpiar_tabla(self):
        self.treeWidget.clear()
        self.filtros.hide()
        self.reiniciar.hide()
        self.pCont.setText("0")

    def iniciar_escaner(self):
        self.capturar.hide()
        self.detener.show()
        self.limpiar_tabla()
        self.paquetes = {}
        
        s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.htons(0x0003))
        cont = 0
        i = 0
        self.iniciado = True
    
        while (self.iniciado):
            
            if (i % 1 == 0):
                cont+=1
                raw = s.recv(2600)
                destino, origen, protocolo, datos = desempaquetar(raw)
                self.paquetes[cont] = [destino, origen, protocolo, datos]
                if(protocolo == 8):
                    prot = 'IPv4'
                    datagrama = cabecera_ipv4(datos)
                    tipo = datagrama[6]
                    if (tipo == 6):
                        tipo = 'TCP (6)'
                    elif (tipo == 17):
                        tipo = 'UDP (17)'
                    elif (tipo == 1):
                        tipo = 'ICMP (1)'
                    else:
                        tipo = 'Desconocido'
                        
                elif (protocolo == 56710):
                    prot = 'IPv6'
                    datagrama = cabecera_ipv6(datos)
                    tipo = datagrama[2]
                    if (tipo == 6):
                        tipo = 'TCP (6)'
                    elif (tipo == 17):
                        tipo = 'UDP (17)'
                    elif (tipo == 58):
                        tipo = 'ICMP (58)'
                    else:
                        tipo = 'Desconocido'
                    
                elif (protocolo == 1544):
                    prot = 'ARP'
                    datagrama = paquete_arp(datos)
                    tipo = datagrama[4]
                    
                else:
                    prot = 'Desconocido'
                    tipo = 'Desconocido'
                    
                self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                self.pCont.setText( str(cont) )
            QCoreApplication.processEvents()
            i += 1
        self.filtros.show()
        self.reiniciar.show()
        
    def cargar_visor(self):
        self.visor = VisorPaquetes()
        self.visor.exec_()
        
    def salir_principal(self):
        self.close()
        
    def guardar_archivo(self):
        nombre_archivo = QFileDialog.getSaveFileName(self,
                "Guardar Captura de paquetes", '.',
                "Captura (*.ivn)")
        if not nombre_archivo:
            return
  
        try:
            archivo_salida = open(str(nombre_archivo[0]+".ivn"), 'wb')
        except IOError:
            QMessageBox.information(self, "Error", "No ha sido posible guardar el archivo")
            return
        QMessageBox.information(self, "Exito", "Captura guardada con éxito")
        pickle.dump(self.paquetes, archivo_salida)
        archivo_salida.close()
        
    def abrir_archivo(self):
        nombre_archivo = QFileDialog.getOpenFileName(self,
                "Abrir captura de paquetes", '',
                "Captura (*.ivn)")
  
        if not nombre_archivo:
            return
  
        try:
            archivo_entrada = open(str(nombre_archivo[0]), 'rb')
        except IOError:
            QMessageBox.information(self, "Error",
                    "Ha ocurrido un error al abrir el archivo")
            return
        
        paqCarga = pickle.load(archivo_entrada)
        cont = 0
        self.paquetes.clear()
        self.limpiar_tabla()
        
        while (cont < len(paqCarga) ):
            cont+=1
            self.paquetes[cont] = paqCarga[cont]
            destino, origen, prot, datos = self.paquetes[cont]
            
            if(prot == 8):
                prot = 'IPv4'
                datagrama = cabecera_ipv4(datos)
                tipo = datagrama[6]
                if (tipo == 6):
                    tipo = 'TCP (6)'
                elif (tipo == 17):
                    tipo = 'UDP (17)'
                elif (tipo == 1):
                    tipo = 'ICMP (1)'
                else:
                    tipo = 'Desconocido'
                    
            elif (prot == 56710):
                prot = 'IPv6'
                datagrama = cabecera_ipv6(datos)
                tipo = datagrama[2]
                if (tipo == 6):
                    tipo = 'TCP (6)'
                elif (tipo == 17):
                    tipo = 'UDP (17)'
                elif (tipo == 58):
                    tipo = 'ICMP (58)'
                else:
                    tipo = 'Desconocido'
                    
            elif (prot == 1544):
                prot = 'ARP'
                datagrama = paquete_arp(datos)
                tipo = datagrama[4]
                
            else:
                prot = 'Desconocido'
                tipo = 'Desconocido'
                    
            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))

        
        self.pCont.setText( str(len(self.paquetes)) )
        QMessageBox.information(self, "Correcto",
                    "La Captura se ha cargado con éxito")
        self.filtros.show()
        self.reiniciar.show()
        archivo_entrada.close()
        
    def aplicar_filtro(self):
        self.limpiar_tabla()
        cont = 0
        contador_paquete_filtro = 0

        while (cont < len(self.paquetes) ):
            cont+=1
            destino, origen, protocolo, datos = self.paquetes[cont]
            
            if(protocolo == 8):
                prot = 'IPv4'
                datagrama = cabecera_ipv4(datos)
                tipo = datagrama[6]
                if (tipo == 6):
                    tipo = 'TCP (6)'
                elif (tipo == 17):
                    tipo = 'UDP (17)'
                elif (tipo == 1):
                    tipo = 'ICMP (1)'
                else:
                    tipo = 'Desconocido'
                    
            elif (protocolo == 56710):
                prot = 'IPv6'
                datagrama = cabecera_ipv6(datos)
                tipo = datagrama[2]
                if (tipo == 6):
                    tipo = 'TCP (6)'
                elif (tipo == 17):
                    tipo = 'UDP (17)'
                elif (tipo == 58):
                    tipo = 'ICMP (58)'
                else:
                    tipo = 'Desconocido'
                
            elif (protocolo == 1544):
                prot = 'ARP'
                datagrama = paquete_arp(datos)
                tipo = datagrama[4]
                
            else:
                prot = 'Desconocido'
                tipo = 'Desconocido'
            
            #Muestra Todos los paquetes
            if (self.rTodos.isChecked() == True):
                if (self.rTodosT.isChecked() == True):
                    contador_paquete_filtro+=1
                    self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                elif (self.rICMP.isChecked() == True):
                    if (tipo == 'ICMP (1)' or tipo == 'ICMP (58)'):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                        
                elif (self.rTCP.isChecked() == True):
                   if (tipo == 'TCP (6)'):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
                        
                elif (self.rUDP.isChecked() == True):
                   if (tipo == 'UDP (17)'):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
             
            #Muesta sólo IPV4
            elif (self.rIPV4.isChecked() == True):
                if (protocolo == 8):
                    if (self.rTodosT.isChecked() == True):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                    elif (self.rICMP.isChecked() == True):
                        if (tipo == 'ICMP (1)' or tipo == 'ICMP (58)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                            
                    elif (self.rTCP.isChecked() == True):
                       if (tipo == 'TCP (6)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
                            
                    elif (self.rUDP.isChecked() == True):
                       if (tipo == 'UDP (17)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
                    
            #Muestra sólo IPv6
            elif (self.rIPV6.isChecked() == True):
                if (protocolo == 56710):
                    if (self.rTodosT.isChecked() == True):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                    elif (self.rICMP.isChecked() == True):
                        if (tipo == 'ICMP (1)' or tipo == 'ICMP (58)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                            
                    elif (self.rTCP.isChecked() == True):
                       if (tipo == 'TCP (6)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
                            
                    elif (self.rUDP.isChecked() == True):
                       if (tipo == 'UDP (17)'):
                            contador_paquete_filtro+=1
                            self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] )) 
                
            #Muestra sólo ARP
            elif (self.rARP.isChecked() == True):
                if (protocolo == 1544):
                    if (self.rTodosT.isChecked() == True):
                        contador_paquete_filtro+=1
                        self.treeWidget.addTopLevelItem(QTreeWidgetItem( [str(cont), str(destino), str(origen), str(prot), str(tipo)] ))
                
        self.filtros.show()
        self.reiniciar.show()
        self.pCont.setText(str(contador_paquete_filtro))

            
        
        
class VisorPaquetes(QDialog):
    paquetes = {}
    seleccion = 0
    codigo_paquete = {}
    tipo_paquete = {}
    
    def __init__(self):
        QWidget.__init__(self)
        self.paquetes = _principal.paquetes
        self.seleccion = int(_principal.seleccion) 
               

        destino, origen, tipo, datos = self.paquetes[self.seleccion]
        if (tipo == 8):
            self.codigo_paquete = cabecera_ipv4(datos)
            if (self.codigo_paquete[6]== 1):
                self.tipo_paquete = paquete_icmp4(self.codigo_paquete[10])
                uic.loadUi("interfaces/ethipv4ICMP.ui",self)
                
            elif (self.codigo_paquete[6] == 6):
                self.tipo_paquete = paquete_tcp(self.codigo_paquete[10])
                uic.loadUi("interfaces/ethipv4TCP.ui",self)
            
            elif (self.codigo_paquete[6] == 17):
                self.tipo_paquete = paquete_udp(self.codigo_paquete[10])
                uic.loadUi("interfaces/ethipv4UDP.ui",self)
            
            else:
                uic.loadUi("interfaces/ethipv4OTRO.ui",self)
                
        elif (tipo == 56710):
            self.codigo_paquete = cabecera_ipv6(datos)
            
            if (self.codigo_paquete[2] == 6):
                self.tipo_paquete = paquete_tcp(self.codigo_paquete[6])
                uic.loadUi("interfaces/ethipv6TCP.ui",self)
                
            elif (self.codigo_paquete[2] == 17):
                self.tipo_paquete = paquete_udp(self.codigo_paquete[6])
                uic.loadUi("interfaces/ethipv6UDP.ui",self)
            
            elif (self.codigo_paquete[2] == 58):
                self.tipo_paquete = paquete_icmp6(self.codigo_paquete[6])
                uic.loadUi("interfaces/ethipv6ICMP.ui",self)
            else:
                uic.loadUi("interfaces/ethipv6OTRO.ui",self)

        elif (tipo == 1544):
            uic.loadUi("interfaces/etharp.ui", self)
            self.codigo_paquete = paquete_arp(datos)
        
        else:
            uic.loadUi("interfaces/ethOTRO.ui", self)
            
        self.cargar_datos_paquete()
        self.cerrar.clicked.connect(self.salir_visor)

        
    def cargar_datos_paquete(self):
        self.titulo.setText("Paquete {}".format(self.seleccion) )
        destino, origen, tipo, datos = self.paquetes[self.seleccion]
        self.macDes.setText(str(destino))
        self.macOri.setText(str(origen))
        
        
        if (tipo == 8):
            self.ethTipo.setText("IPv4 ({})".format(str(tipo)))
            self.version.setText( str(self.codigo_paquete[0]) )
            self.dscp.setText( str(self.codigo_paquete[1]) )
            self.longitud.setText( str(self.codigo_paquete[2]) )
            self.banderas.setText( str(self.codigo_paquete[4]) )
            self.tiempo_vida.setText( str(self.codigo_paquete[5]) )
            self.sum_comp.setText( str(self.codigo_paquete[7]) )
            self.IP_origen.setText( str(self.codigo_paquete[8]) )
            self.IP_destino.setText( str(self.codigo_paquete[9]) ) 
            
            #ICMP IPV4    
            if (self.codigo_paquete[6] == 1):
                self.tipo.setText( str(self.tipo_paquete[0]) )
                self.codigo.setText( str(self.tipo_paquete[1]) )
                self.checksum.setText( str(self.tipo_paquete[2]) )
                self.textDatos.setText( self.tipo_paquete[3] )
                self.protocolo.setText( "ICMP ({})".format(str(self.codigo_paquete[6]) ))
                self.tipoT.setText( tipo_icmp4(self.tipo_paquete[0]))
                
            #TCP IPv4
            elif (self.codigo_paquete[6] == 6):
                self.puerto_origen.setText( str(self.tipo_paquete[0]) )
                self.puerto_destino.setText( str(self.tipo_paquete[1]) )
                self.secuencia.setText( str(self.tipo_paquete[2]) )
                self.num_confirmacion.setText( str(self.tipo_paquete[3]) )
                self.urg.setText( str(self.tipo_paquete[4]) )
                self.ack.setText( str(self.tipo_paquete[5]) )
                self.psh.setText( str(self.tipo_paquete[6]) )
                self.rst.setText( str(self.tipo_paquete[7]) )
                self.syn.setText( str(self.tipo_paquete[8]) )
                self.fin.setText( str(self.tipo_paquete[9]) )
                self.textDatos.setText( self.tipo_paquete[10] )
                self.protocolo.setText( "TCP ({})".format(str(self.codigo_paquete[6]) ))
                
            #UDP IPv4    
            elif (self.codigo_paquete[6] == 17):
                self.puerto_origen.setText( str(self.tipo_paquete[0]) )
                self.puerto_destino.setText( str(self.tipo_paquete[1]) )
                self.longitudUDP.setText( str(self.tipo_paquete[2]) )
                self.checksum.setText( str(self.tipo_paquete[3]) )
                self.textDatos.setText( str(self.tipo_paquete[4]) )
                self.protocolo.setText( "UDP ({})".format(str(self.codigo_paquete[6]) ))

            else:
                self.protocolo.setText( "Desconocido ({})".format(str(self.codigo_paquete[6]) ))
                self.textDatos.setText( str(self.codigo_paquete[10]) )
                
        elif (tipo == 56710):
            self.ethTipo.setText("IPv6 ({})".format(str(tipo)))
            self.version.setText( str(self.codigo_paquete[0]) )
            self.carga_util.setText( str(self.codigo_paquete[1]) )
            self.limite_saltos.setText( str(self.codigo_paquete[3]) )
            self.IP_origen.setText( str(self.codigo_paquete[4]) )
            self.IP_destino.setText( str(self.codigo_paquete[5]) )
            
            
            if (self.codigo_paquete[2] == 6 ):
                self.puerto_origen.setText( str(self.tipo_paquete[0]) )
                self.puerto_destino.setText( str(self.tipo_paquete[1]) )
                self.secuencia.setText( str(self.tipo_paquete[2]) )
                self.num_confirmacion.setText( str(self.tipo_paquete[3]) )
                self.urg.setText( str(self.tipo_paquete[4]) )
                self.ack.setText( str(self.tipo_paquete[5]) )
                self.psh.setText( str(self.tipo_paquete[6]) )
                self.rst.setText( str(self.tipo_paquete[7]) )
                self.syn.setText( str(self.tipo_paquete[8]) )
                self.fin.setText( str(self.tipo_paquete[9]) )
                self.textDatos.setText( self.tipo_paquete[10] )
                self.cabe_sig.setText( "TCP ({})".format(str(self.codigo_paquete[2]) ))
                
            elif (self.codigo_paquete[2] == 17):
                self.puerto_origen.setText( str(self.tipo_paquete[0]) )
                self.puerto_destino.setText( str(self.tipo_paquete[1]) )
                self.longitudUDP.setText( str(self.tipo_paquete[2]) )
                self.checksum.setText( str(self.tipo_paquete[3]) )
                self.textDatos.setText( str(self.tipo_paquete[4]) )
                self.cabe_sig.setText( "UDP ({})".format(str(self.codigo_paquete[2]) ))
                
            elif (self.codigo_paquete[2] == 58):
                self.tipo.setText( str(self.tipo_paquete[0]) )
                self.codigo.setText( str(self.tipo_paquete[1]) )
                self.checksum.setText( str(self.tipo_paquete[2]) )
                self.textDatos.setText( self.tipo_paquete[3] )
                self.cabe_sig.setText( "ICMP ({})".format(str(self.codigo_paquete[2]) ))
                self.tipoT.setText(tipo_icmp6(self.tipo_paquete[0]) )
            
            else:
                self.cabe_sig.setText( "Desconocido ({})".format(self.codigo_paquete[2]))
            
        elif (tipo == 1544):
            self.ethTipo.setText("ARP ({})".format(str(tipo)))
            self.tipo_hardware.setText( str(self.codigo_paquete[0]) )
            self.tipo_protocolo.setText( str(self.codigo_paquete[1]) )
            self.longitud_hw.setText( str(self.codigo_paquete[2]) )
            self.longitud_prot.setText( str(self.codigo_paquete[3]) )
            self.operacion.setText( str(self.codigo_paquete[4]) )
            self.dir_hw_origen.setText( str(self.codigo_paquete[5]) )
            self.IP_hw_origen.setText( str(self.codigo_paquete[6]) )
            self.dir_hw_destino.setText( str(self.codigo_paquete[7]) )
            self.IP_hw_destino.setText( str(self.codigo_paquete[8]) )

        else:
            self.ethTipo.setText("Desconocido ({})".format(str(tipo)))
            self.textDatos( str(hexdump(datos)) )
                
    def salir_visor(self):
        self.close()

class AcercaDe(QDialog):
    def __init__(self):
        QMainWindow.__init__(self)
        uic.loadUi("interfaces/acercade.ui", self)
        

app = QApplication(sys.argv)
_principal = Principal()
_principal.show()
sys.exit(app.exec_())
